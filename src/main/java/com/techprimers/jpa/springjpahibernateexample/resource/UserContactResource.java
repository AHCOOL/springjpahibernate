package com.techprimers.jpa.springjpahibernateexample.resource;


import com.techprimers.jpa.springjpahibernateexample.model.UserContact;
import com.techprimers.jpa.springjpahibernateexample.model.Users;
import com.techprimers.jpa.springjpahibernateexample.model.UsersLog;
import com.techprimers.jpa.springjpahibernateexample.respository.UsersContactRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/rest/userscontact")
@RestController
public class UserContactResource {

    private UsersContactRepository usersContactRepository;


    //if we use this account contructor we not need to use @Autowire
    public UserContactResource(UsersContactRepository usersContactRepository) {
        this.usersContactRepository = usersContactRepository;
    }

    @GetMapping(value = "/all")
    public List<UserContact> getUsersContact(){
        return usersContactRepository.findAll();
    }

    @GetMapping(value = "/update/{name}")
    public List<UserContact> update(@PathVariable final String name){

        UserContact userContact = new UserContact();
        Users users = new Users();

        UsersLog usersLog = new UsersLog();
        usersLog.setLog("HI Youtube");

        UsersLog usersLog2 = new UsersLog();
        usersLog2.setLog("HI Viewer");

        users.setTeamName("Developer")
                .setSalary(1000)
                .setName(name)
                .setUsersLogs(Arrays.asList(usersLog, usersLog2));

        userContact.setPhoneNo(11111)
                .setUsers(users);

        usersContactRepository.save(userContact);

        return usersContactRepository.findAll();
    }
}
