package com.techprimers.jpa.springjpahibernateexample.model;


import javax.persistence.*;

@Entity
@Table(name = "users_contact", catalog = "test")
public class UserContact {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    private Integer phoneNo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private Users users;

    public UserContact(){

    }

    public Users getUsers() {
        return users;
    }

    public UserContact setUsers(Users users) {
        this.users = users;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public UserContact setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getPhoneNo() {
        return phoneNo;
    }

    public UserContact setPhoneNo(Integer phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    @Override
    public String toString() {
        return "UserContact{" +
                "id=" + id +
                ", phoneNo=" + phoneNo +
                '}';
    }
}
